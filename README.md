# GameJame G4 2019


[Download GIT]

https://git-scm.com/

[Initialisation de la SSH Key]

Cette opération est à faire si vous n'avez pas encore configuré git

- Ouvrir git bash
- Taper la commande suivante (en remplaçant par votre mail) ssh-keygen -t rsa -b 4096 -C "email@example.com"
- Ensuite plusieurs lignes vont apparaitre cliquer sur entrée pour toutes
- Se rendre ensuite sur le dossier .ssh qui s'est crée sur votre ordi
- Ouvrir le fichier id_rsa.pub avec un editeur de text
- Puis faire ctrl a et ctrl c
- Se rendre après dans les settings de votre profil gitlab
- Cliquer sur SSH Keys sur la gauche
- Dans le champs Key collé la clé copier plûtot
- Dans le champ title mettez le nom de votre ordi
- Puis cliquer sur add
- Et c'est bon.

[Mise en place du projet]

- Créer un dossier sur votre ordi ou vous voulez
- Lancer git bash (clique droit git bash Here)
- Coller la commande "git clone git@gitlab.com:quent.kent069/gamejame-g4-2019.git"
- Taper la commande "pull pour être sur"
- Ensuite taper les commandes pour configurer l'autorisation d'accès au gitlab en tapant les commandes :
- git congif --global user.email "email@example.com"
- git config --global user.name "Your name" (ton pseudo gitlab)
